/*********** ProgrammaConTab.java ***********************
 * Questa classe permette la costruzione di un programma
 * a cui associo una tabella dei simboli per la risoluzione
 * dei valori delle variabili presenti.
 *********************************************************/
class ProgrammaConTab {

  private Programma programma;
  private SymbolTable tabella;

  public ProgrammaConTab(Programma p, SymbolTable s) {
    programma = p;
    tabella = s;
  }

  public Programma getProgramma() {
    return programma;
  }

  public SymbolTable getSymbolTable() {
    return tabella;
  }
}
