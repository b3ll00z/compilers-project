/*********** AnalizzatoreLessicaleTester.java ***************
 *
 * Classe creata appositamente per testare il corretto riconoscimento
 * dei token forniti dall'analizzatore lessicale.
 * All'interno del metodo main viene istanziato l'analizzatore lessicale
 * invocando il costruttore della classe Scanner. Tale classe viene generata
 * automaticamente da JFlex.
 * La sorgente di input e' un file il cui nome viene specificato
 * sulla riga di comando. Se non viene specificato il nome la
 * lettura avviene da tastiera.
 *
 ************************************************************/

import java.io.*;
import java_cup.runtime.*;

public class AnalizzatoreLessicaleTester {
  public static void main(String[] args) throws Exception {
    // creazione della symbol factory
    ComplexSymbolFactory sf = new ComplexSymbolFactory();
    Scanner scanner;

    if (args.length == 0) scanner = new Scanner(new InputStreamReader(System.in), sf);
    else scanner = new Scanner(new FileReader(args[0]), sf);

    Symbol token;

    while ((token = scanner.next_token()).sym != ParserSym.EOF)
      System.out.println(token + ((token.value != null) ? " " + token.value : ""));
  }
}
