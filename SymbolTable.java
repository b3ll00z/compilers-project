/**************** SymbolTable.java ********************
 * Classe per la creazione di una tabella dei simboli
 * usata per la gestione delle variabili, per operazioni
 * di reperimento e aggiunta.
 *******************************************************/
import java.util.Iterator;
import java.util.Vector;

public class SymbolTable implements Iterable<Descrittore> {

  private Vector<Descrittore> tabella;
  private int prossimoIndirizzo = 2;

  /* Costruisce tabella vuota */
  public SymbolTable() {
    tabella = new Vector<Descrittore>();
  }

  /* Cerca il descrittore di una stringa nella tabella, se assente restituisce null */
  public Descrittore trova(String s) {
    int posizione = tabella.indexOf(new Descrittore(s));
    if (posizione == -1) return null;
    else return tabella.elementAt(posizione);
  }

  /* Aggiunge un descrittore alla tabella fornendole un indirizzo dato
  dal campo prossimoIndirizzo */
  public void aggiungi(Descrittore d) {
    tabella.add(d);
    prossimoIndirizzo = d.assegnaIndirizzo(prossimoIndirizzo);
  }

  /* Cerca il descrittore di una stringa nella tabella, se non c'e' lo aggiunge */
  public Descrittore trovaEAggiungi(String s) {
    Descrittore d = trova(s);
    if (d == null) {
      d = new Descrittore(s);
      aggiungi(d);
    }
    return d;
  }

  public Iterator<Descrittore> iterator() {
    return tabella.iterator();
  }
}
