# COMPILERS PROJECT
Tutti i sorgenti .java per la realizzazione del compilatore sono reperibili nella directory corrente.

## Cartelle
`sorgenti/`
- Contiene i sorgenti su cui ho testato il compilatore

`dumpSorgenti/`
- Contiene i rispettivi codici tradotti

`lib/`
- Librerie di supporto alla generazione degli analizzatori lessicali e sintattici e della macchina virtuale su cui eseguire il codice tradotto

## File
`Compilatore.java`
- Richiede come argomento il nome del file sorgente
- Provvede alla generazione del codice compatibile con macchina a stack

`AnalizzatoreLessicaleTester.java`
- Programma per stampare ogni token riconosciuto sulla base della specifica lessicale
con cui è stato prodotto lo scanner

`Programma.java, Istruzione.java, Espressione.java`
- Classi adoperate per la costruzione dell'albero sintattico
- Rifacendomi ai codici forniti nelle lezioni scorse, ho deciso di attuare lo 
stesso meccanismo del compilatore per espressioni aritmetiche
- In pratica viene messo a disposizione dalla superclasse (astratta) `Programma`
il metodo `generaCodice()`, che viene opportunamente implementato nelle rispettive
estensioni

`Descrittore.java, SymbolTable.java`
- Classi per la gestione della tabella dei simboli. Ogni variabile viene gestita
da un descrittore, cui attributi sono un nome (quello dell'identificatore) e
l'indirizzo di collocazione nello stack
- SymbolTable organizza tutte le variabili (descrittori) incontrate durante
la compilazione all'interno di un'istanza della classe `Vector`

`ProgrammaConTab.java`
- Permette di raccogliere il parse tree del programma sorgente con la tabella dei
simboli. Viene usata in fase di compilazione per la generazione del codice target

`specificaSintattica.cup`
- File di specifica sintattica, che definisce la costruzione dell'albero e la
gestione della symbol table, sulla base delle precedenze e associatività 
contemplate in Java e della grammatica fornita dal testo del progetto

`specificaLessicale.lex`
- File di specifica lessicale per riconoscimento di keyword e più in generale di
elementi lessicali per la costruzione dei token

`Makefile`
- File di build di supporto alle varie fasi
- Alcune regole sono vincolate a dei target

Per generare ed eseguire codice macchina si proceda coi seguenti step:
```sh
$ make parser
$ make scanner
$ make compile
$ make run_compiler SOURCE=<path file>
$ make run_target_code
```

