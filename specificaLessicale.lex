import java_cup.runtime.*;

%% 

%cup
%class Scanner
%unicode
%ignorecase

%{  //codice per associare la Symbol Factory
	ComplexSymbolFactory sf;
    public Scanner(java.io.Reader in, ComplexSymbolFactory sf) {
		this(in);
		this.sf = sf;
    }

  	//metodi per restituire il numero di caratteri, righe e token  
    public int currentLineNumber() {
		return yyline + 1;
    }
%}
%line


FINE_RIGA = \r | \n | \r\n
SPAZIATURA = [ \t\f]
CIFRA = [:digit:]
LETTERA = [:letter:]
HEX_LITERAL = [0-9a-fA-F]

%% 

"+"         {return sf.newSymbol("PIU",    ParserSym.PIU);}
"-"         {return sf.newSymbol("MENO",   ParserSym.MENO);}
"*"         {return sf.newSymbol("PER",    ParserSym.PER);}
"/"         {return sf.newSymbol("DIVISO", ParserSym.DIVISO);}
"%"         {return sf.newSymbol("MODULO", ParserSym.MODULO);}
"(" 	    {return sf.newSymbol("APERTA", ParserSym.TONDA_APERTA);}
")"         {return sf.newSymbol("CHIUSA", ParserSym.TONDA_CHIUSA);}
"="         {return sf.newSymbol("ASSEGNAMENTO", ParserSym.ASSEGNAMENTO);}
"?"         {return sf.newSymbol("QUESTION", ParserSym.QUESTION);}
":"         {return sf.newSymbol("COLON", ParserSym.COLON);}
"loop"      {return sf.newSymbol("BEGIN_LOOP", ParserSym.BEGIN_LOOP);}
"endLoop"   {return sf.newSymbol("END_LOOP", ParserSym.END_LOOP);}
"output"    {return sf.newSymbol("OUTPUT", ParserSym.OUTPUT);}
"input"     {return sf.newSymbol("INPUT", ParserSym.INPUT);}
"newLine"   {return sf.newSymbol("NEWLINE", ParserSym.NEWLINE);}

^{FINE_RIGA} {}
{FINE_RIGA}	 {return sf.newSymbol("CR", ParserSym.CR);}

"&".*{FINE_RIGA}"//".* {return sf.newSymbol("err", ParserSym.error);}
"&".*{FINE_RIGA}       { }

"//".*{FINE_RIGA}      { }
			  	
"\"".+"\""  { String tokenLetto = yytext(); // Restituisco la stringa senza ""
			  String stringa = tokenLetto.substring(1, tokenLetto.length() - 1); 
			  return sf.newSymbol("STRINGA", ParserSym.STRINGA, stringa); }

"0x"({HEX_LITERAL})+ { String esadecimale = yytext().substring(2);
					   Integer n = new Integer(Integer.parseInt(esadecimale, 16));
					   return sf.newSymbol("NUMERO", ParserSym.NUMERO, n); }

{CIFRA}+    {return sf.newSymbol("NUMERO", ParserSym.NUMERO, new Integer(yytext()));}

{LETTERA}({LETTERA}|{CIFRA})* {return sf.newSymbol("IDENT", ParserSym.IDENT, yytext());}

{SPAZIATURA} { }

.            {return sf.newSymbol("err", ParserSym.error);}

<<EOF>>      {return sf.newSymbol("EOF", ParserSym.EOF);}

