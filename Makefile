.PHONY: clear clear_analizers clear_classes

parser: specificaSintattica.cup
	java -jar lib/java-cup-11b.jar specificaSintattica.cup
	
scanner: specificaLessicale.lex Parser.java ParserSym.java
	java -jar lib/jflex-1.6.1.jar specificaLessicale.lex 
	
compile: Scanner.java Parser.java ParserSym.java
	javac -cp lib/java-cup-11b.jar:lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. *.java 

run_lexical_tester: AnalizzatoreLessicaleTester.class	
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. AnalizzatoreLessicaleTester $(SOURCE)
	
run_compiler: Compilatore.class
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. Compilatore $(SOURCE)

run_target_code: eseguibile
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. lt.macchina.Macchina eseguibile
	
dump_target_code: eseguibile
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. lt.macchina.Macchina -L eseguibile > eseguibile.dump

dump_and_run_target_code: eseguibile
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. lt.macchina.Macchina -l eseguibile

debug_target_code: eseguibile
	java -cp lib/java-cup-11b-runtime.jar:lib/ltMacchina.jar:. lt.macchina.Macchina -d eseguibile

clear_analizers:
	rm Scanner.java Parser.java ParserSym.java

clear_classes:
	rm *.class

clear:
	rm *.class Scanner.java Parser.java ParserSym.java
