/******************** Descrittore.java *********************
 * Ogni istanza di questa classe descrive un identificatore.
 * Un identificatore viene trattato tramite il suo indirizzo,
 * che viene assegnato in fase di assegnamento di un valore
 ************************************************************/
public class Descrittore {

  private String identificatore;
  private int indirizzo;
  private int lunghezza = 1;

  public Descrittore(String id, int val) {
    identificatore = id;
    indirizzo = val;
  }

  public Descrittore(String id) {
    this(id, 0);
  }

  public String getIdentificatore() {
    return identificatore;
  }

  public int getIndirizzo() {
    return indirizzo;
  }

  public int getLunghezza() {
    return lunghezza;
  }

  public int assegnaIndirizzo(int ind) {
    indirizzo = ind;
    return indirizzo + lunghezza;
  }

  public boolean equals(Descrittore d) {
    return this.identificatore.equals(d.identificatore);
  }

  public boolean equals(Object o) {
    if (o instanceof Descrittore) return equals((Descrittore) o);
    else return false;
  }

  public String toString() {
    return identificatore + " @ " + indirizzo;
  }
}
