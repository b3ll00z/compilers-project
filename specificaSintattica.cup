// File di specifica JavaCup
/* Obiettivo: 
   Ho un programma scritto con le specifiche sintattiche
   del progetto. Da questo programma voglio generare il
   corrispondente albero sintattico */ 

import java_cup.runtime.*;

class Parser;    //nome da attribuire al parser (classe generata)

action code {:
	SymbolTable symbolTable = new SymbolTable();
:}

parser code{:
	/* Ridefinizione del metodo che visualizza i messaggi di errore */
	public void unrecovered_syntax_error(Symbol cur_token) throws java.lang.Exception {
		Scanner sc = (Scanner) getScanner(); //riferimento all’analizzatore
		//lessicale in uso
		report_fatal_error("Errore di sintassi alla riga " + sc.currentLineNumber() 
					+ " leggendo " + sc.yytext(), null);
		//numero della riga in esame e testo corrispondente al token corrente
	}
:}


/* Simboli terminali (token restituiti dallo scanner). */
terminal			ASSEGNAMENTO;
terminal			OUTPUT, INPUT, NEWLINE;
terminal			BEGIN_LOOP, END_LOOP;
terminal			CR;
terminal			QUESTION, COLON;
terminal			PIU, MENO, PER, DIVISO, MODULO;
terminal			UNARIO, TONDA_APERTA, TONDA_CHIUSA;
terminal Integer	NUMERO;
terminal String    	IDENT;
terminal String	   	STRINGA;


/* Non terminali */
non terminal ProgrammaConTab 	 programmaConTab;
non terminal Programma 		 	 programma;
non terminal SequenzaIstruzioni	 seqIstruzioni;
non terminal Istruzione  	 	 istruzione;
non terminal Istruzione		 	 assegnamento;
non terminal Istruzione		 	 scrittura;
non terminal Istruzione		     ripetizione;
non terminal Espressione 	 	 espressione;

/* Precedenze e associativita' */
precedence right 	 ASSEGNAMENTO;
precedence right 	 QUESTION, COLON;
precedence left 	 PIU, MENO;
precedence left		 PER, DIVISO, MODULO;
precedence nonassoc  UNARIO;

/* Simbolo iniziale */
start with programmaConTab;

/* Produzioni */
programmaConTab ::= programma:p
                    {: RESULT = new ProgrammaConTab(p, symbolTable); :};

programma ::= seqIstruzioni:seq {: RESULT = seq; :};

seqIstruzioni ::= 	istruzione:i CR
					{: RESULT = new SequenzaIstruzioni(i); :}

				 	| 

					seqIstruzioni:seq istruzione:i CR
				 	{: RESULT = new SequenzaIstruzioni(seq, i); :} 
					;

istruzione ::= 	assegnamento:a 
				{: RESULT = a; :}
				
				| 
				
				scrittura:s
				{: RESULT = s; :}
			    
				| 
				
				ripetizione:r 
			    {: RESULT = r; :}
				;

assegnamento ::= IDENT:id ASSEGNAMENTO espressione:e
				 {:	Descrittore d = symbolTable.trovaEAggiungi(id); 
				    RESULT = new Assegnamento(d, e); :}
				 ;
		
scrittura ::= 	OUTPUT STRINGA:s espressione:e
	      	  	{: RESULT = new Scrittura(s, e); :}
              	
				| 
				
				OUTPUT STRINGA:s 
	      		{: RESULT = new Scrittura(s); :}
				
				| 
				
				OUTPUT espressione:e 
		  		{: RESULT = new Scrittura(e); :}
				
				| 
				
				NEWLINE
				{: RESULT = new Scrittura(); :} 
				;

ripetizione ::= BEGIN_LOOP espressione:e CR seqIstruzioni:seq END_LOOP
				{: RESULT = new Ripetizione(e, seq); :};
	
espressione ::=	NUMERO:n 
  				{: RESULT = new Numero(n); :}  
				
				| 
				
				IDENT:id
				{: Descrittore d = symbolTable.trova(id); 
				 // se sto per usare una variabile non dichiarata
				 //	lancio un'eccezione 
				  	 
					if (d == null)
						unrecovered_syntax_error(null);
				  	else
		      		 	RESULT = new Identificatore(d); 
		      	:}
				
				|
				
				espressione:e1 PIU espressione:e2 
		  		{: RESULT = new Piu(e1, e2); :} 
	      		
				|
				
				espressione:e1 MENO espressione:e2 
		  		{: RESULT = new Meno(e1, e2); :} 
				
				|
	      		
				espressione:e1 PER espressione:e2  
		  		{: RESULT = new Per(e1, e2); :} 
				
				|
	      		
				espressione:e1 DIVISO espressione:e2 
		  		{: RESULT = new Diviso(e1, e2); :} 
		      	
				| 
				
				espressione:e1 MODULO espressione:e2 
		  		{: RESULT = new Modulo(e1, e2); :}
				
				| 
				
				MENO espressione:e
		  		{: RESULT = new UnMeno(e); :} %prec UNARIO
				
				|
	      		
				PIU espressione:e 
		  		{: RESULT = new UnPiu(e); :}  %prec UNARIO
				
				|
	      		
				TONDA_APERTA espressione:e TONDA_CHIUSA 
		  		{: RESULT = e; :} 
				
				|

				IDENT:id ASSEGNAMENTO espressione:e
				{: Descrittore d = symbolTable.trovaEAggiungi(id);
				   RESULT = new AssegnamentoEspr(d, e); :}

				|

				espressione:e1 QUESTION espressione:e2 COLON espressione:e3	
				{: RESULT = new Ternario(e1, e2, e3); :}

				|

				INPUT
				{: RESULT = new Input(); :}	      	

				|

				INPUT STRINGA:s
				{: RESULT = new Input(s); :} 
				;
