/*************** Espressione.java *************************
 * Classe dedicata alla creazione di nodi per il parse tree
 * inerenti alle derivazioni a destra della segunte produzione
 *
 * espressione -> numero | identificatore
 * | espressione + espressione
 * | espressione - espressione
 * | espressione * espressione
 * | espressione / espressione
 * | espressione % espressione
 * | -espressione
 * | +espressione
 * | (espressione)
 * | identificatore = espressione
 * | espressione ? espressione : espressione
 * | input
 * | input stringa
 *******************************************************/
import static lt.macchina.Macchina.*;

import lt.macchina.Codice;

public abstract class Espressione extends Programma {}

class AssegnamentoEspr extends Espressione {
  private Descrittore descrittore;
  private Espressione espressione;

  public AssegnamentoEspr(Descrittore d, Espressione e) {
    descrittore = d;
    espressione = e;
  }

  /* Questo assegnamento avviene all'interno di un'espressione,
  pertanto rimetto sullo stack il valore che ho messo all'indirizzo
  del descrittore */
  public void generaCodice(Codice c) {
    espressione.generaCodice(c);
    c.genera(POP, descrittore.getIndirizzo());
    c.genera(PUSH, descrittore.getIndirizzo());
  }

  public String toString() {
    return espressione.toString() + descrittore.toString() + " =";
  }
}

class Piu extends Espressione {
  private Espressione sx, dx;

  public Piu(Espressione leftExpr, Espressione rightExpr) {
    sx = leftExpr;
    dx = rightExpr;
  }

  public void generaCodice(Codice c) {
    sx.generaCodice(c);
    dx.generaCodice(c);
    c.genera(ADD);
  }

  public String toString() {
    return sx.toString() + " " + dx.toString() + " +";
  }
}

class Meno extends Espressione {
  private Espressione sx, dx;

  public Meno(Espressione leftExpr, Espressione rightExpr) {
    sx = leftExpr;
    dx = rightExpr;
  }

  public void generaCodice(Codice c) {
    sx.generaCodice(c);
    dx.generaCodice(c);
    c.genera(SUB);
  }

  public String toString() {
    return sx.toString() + " " + dx.toString() + " -";
  }
}

class Per extends Espressione {
  private Espressione sx, dx;

  public Per(Espressione leftExpr, Espressione rightExpr) {
    sx = leftExpr;
    dx = rightExpr;
  }

  public void generaCodice(Codice c) {
    sx.generaCodice(c);
    dx.generaCodice(c);
    c.genera(MUL);
  }

  public String toString() {
    return sx.toString() + " " + dx.toString() + " *";
  }
}

class Diviso extends Espressione {
  private Espressione sx, dx;

  public Diviso(Espressione leftExpr, Espressione rightExpr) {
    sx = leftExpr;
    dx = rightExpr;
  }

  public void generaCodice(Codice c) {
    sx.generaCodice(c);
    dx.generaCodice(c);
    c.genera(DIV);
  }

  public String toString() {
    return sx.toString() + " " + dx.toString() + " /";
  }
}

class Modulo extends Espressione {
  private Espressione sx, dx;

  public Modulo(Espressione leftExpr, Espressione rightExpr) {
    sx = leftExpr;
    dx = rightExpr;
  }
  /* Per evitare di ricalcolare le espressioni degli operandi tratto gli
  indirizzi 0 e 1 riservati per questa operazione */
  public void generaCodice(Codice c) {
    sx.generaCodice(c);
    dx.generaCodice(c);
    c.genera(POP, 1);
    c.genera(POP, 0);
    c.genera(PUSH, 0);
    c.genera(PUSH, 1);
    c.genera(PUSH, 0);
    c.genera(PUSH, 1);
    c.genera(DIV);
    c.genera(MUL);
    c.genera(SUB);
  }

  public String toString() {
    return sx.toString() + " " + dx.toString() + " %";
  }
}

class UnMeno extends Espressione {
  private Espressione espressione;

  public UnMeno(Espressione p) {
    espressione = p;
  }

  public void generaCodice(Codice c) {
    c.genera(PUSHIMM, 0);
    espressione.generaCodice(c);
    c.genera(SUB);
  }

  public String toString() {
    return espressione.toString() + "-";
  }
}

class UnPiu extends Espressione {
  private Espressione espressione;

  public UnPiu(Espressione p) {
    espressione = p;
  }

  public void generaCodice(Codice c) {
    espressione.generaCodice(c);
  }

  public String toString() {
    return espressione.toString() + "+";
  }
}

class Ternario extends Espressione {
  private Espressione condizione, espressioneV, espressioneF;

  public Ternario(Espressione c, Espressione v, Espressione f) {
    condizione = c;
    espressioneV = v;
    espressioneF = f;
  }

  public void generaCodice(Codice c) {
    /* Codice per il condizionale */
    condizione.generaCodice(c);

    /* Se il valore è falso vado al terzo operatore, ancora non so dove */
    int posizioneJZERO = c.generaParziale(JZERO);

    /* Se il valore è vero proseguo con il secondo operatore */
    espressioneV.generaCodice(c);

    /* A questo punto faccio jump incondizionata verso il codice dopo
    il terzo operatore */
    int posizioneFineTernario = c.generaParziale(JUMP);

    /* Chiudo la JZERO inerente al caso negativo del primo operatore */
    int operandoJZERO = c.indirizzoProssimaIstruzione();
    c.completaIstruzione(posizioneJZERO, operandoJZERO);

    /* Proseguo con la generazione del codice del terzo operatore */
    espressioneF.generaCodice(c);

    /* Arrivato qui chiedo l'indirizzo della prossima istruzione in modo
    da completare l'operando del JUMP incondizionato posizionato al
    termine del secondo operatore */
    int operandoJUMP = c.indirizzoProssimaIstruzione();
    c.completaIstruzione(posizioneFineTernario, operandoJUMP);
  }

  public String toString() {
    return condizione.toString()
        + " ?"
        + espressioneV.toString()
        + " "
        + espressioneF.toString()
        + ":";
  }
}

class Numero extends Espressione {
  private Integer numero;

  public Numero(Integer num) {
    numero = num;
  }

  public void generaCodice(Codice c) {
    c.genera(PUSHIMM, numero.intValue());
  }

  public String toString() {
    return numero + "";
  }
}

class Identificatore extends Espressione {
  private Descrittore descrittore;

  public Identificatore(Descrittore d) {
    descrittore = d;
  }

  public void generaCodice(Codice c) {
    c.genera(PUSH, descrittore.getIndirizzo());
  }

  public String toString() {
    return descrittore.getIdentificatore();
  }
}

class Input extends Espressione {
  private String messaggio;

  public Input() {
    this(null);
  }

  public Input(String messaggioInput) {
    messaggio = messaggioInput;
  }

  public void generaCodice(Codice c) {
    if (messaggio != null) {
      for (int i = 0; i < messaggio.length(); i++) {
        c.genera(PUSHIMM, messaggio.charAt(i));
        c.genera(OUTPUTCH);
      }
    }
    c.genera(INPUT);
  }

  public String toString() {
    return messaggio + " input";
  }
}
