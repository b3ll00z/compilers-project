/*********************** Compilatore.java ****************************
 * Questa classe realizza il compilatore da linguaggio dalle specifiche
 * di progetto al linguaggio target compatibile con la macchina a stack
 * reperibile nel package lt.macchina.
 **********************************************************************/
import static lt.macchina.Macchina.*;

import java.io.*;
import java_cup.runtime.*;
import lt.macchina.*;

class Compilatore {

  public static void main(String[] args) throws java.io.IOException {

    if (args[0] == null) {
      System.out.println("Necessario file sorgente in input");
      System.exit(1);
    }

    // creazione della symbol factory
    ComplexSymbolFactory sf = new ComplexSymbolFactory();

    // creazione dell'analizzatore lessicale
    Scanner scanner = new Scanner(new FileReader(args[0]), sf);

    // creazione del parser
    Parser p = new Parser(scanner, sf);

    try {
      Symbol ris = p.parse();
      ProgrammaConTab risultato = (ProgrammaConTab) ris.value;
      Programma albero = risultato.getProgramma();
      SymbolTable tabella = risultato.getSymbolTable();

      Codice c = new Codice("eseguibile");

      /* Allocazione spazio per le variabili a cominciare da 0 e 1,
      riservati per eventuali operazioni di modulo */
      c.genera(PUSHIMM, 0);
      c.genera(PUSHIMM, 0);
      for (Descrittore d : tabella) c.genera(PUSHIMM, 0);

      /* Generazione codice */
      albero.generaCodice(c);

      /* Genera il codice per terminare l'esecuzione */
      c.genera(PUSHIMM, '\n'); // ritorno a capo
      c.genera(OUTPUTCH);
      c.genera(HALT);
      c.fineCodice();

    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
