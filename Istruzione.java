/******************* Istruzione.java ******************************
 * Classe per la rappresentazione delle derivazioni a destra della
 * seguente produzione:
 *
 * istruzione -> assegnamento | scrittura | ripetizione
 *
 * Ogni derivazione ha la sua rispettiva classe per la costruzione
 * del parse tree
 *******************************************************************/
import static lt.macchina.Macchina.*;

import lt.macchina.Codice;

abstract class Istruzione extends Programma {}

class Assegnamento extends Istruzione {
  private Descrittore descrittore;
  private Espressione espressione;

  public Assegnamento(Descrittore d, Espressione e) {
    descrittore = d;
    espressione = e;
  }
  /* A differenza dell'assegnamento all'interno di un'espressione in questo
  caso non rimetto sullo stack il valore che ho assegnato al descrittore */
  public void generaCodice(Codice c) {
    espressione.generaCodice(c);
    c.genera(POP, descrittore.getIndirizzo());
  }
}

class Scrittura extends Istruzione {
  private String messaggio;
  private Espressione espressione;

  public Scrittura() {
    this(null, null);
  }

  public Scrittura(String messaggio) {
    this(messaggio, null);
  }

  public Scrittura(Espressione espressione) {
    this(null, espressione);
  }

  public Scrittura(String m, Espressione e) {
    messaggio = m;
    espressione = e;
  }

  public void generaCodice(Codice c) {
    /* Generazione codice per keyword newLine */
    if (messaggio == null && espressione == null) {
      c.genera(PUSHIMM, '\n');
      c.genera(OUTPUTCH);
      return;
    }

    if (messaggio != null) {
      for (int i = 0; i < messaggio.length(); i++) {
        c.genera(PUSHIMM, messaggio.charAt(i));
        c.genera(OUTPUTCH);
      }
    }

    if (espressione != null) {
      espressione.generaCodice(c);
      c.genera(OUTPUT);
    }
  }
}

class Ripetizione extends Istruzione {
  private Espressione condizione;
  private SequenzaIstruzioni sequenza;

  public Ripetizione(Espressione cond, SequenzaIstruzioni istruzioni) {
    condizione = cond;
    sequenza = istruzioni;
  }

  public void generaCodice(Codice c) {
    /* Acquisico punto d'inizio della condizione del loop */
    int inizioLoop = c.indirizzoProssimaIstruzione();
    condizione.generaCodice(c);

    /* In caso di condizione falsa esco dal ciclo, ma ancora non so dove */
    int posizioneJZERO = c.generaParziale(JZERO);
    sequenza.generaCodice(c);

    /* Terminata sequenza istruzioni torno all'inizio del loop */
    c.genera(JUMP, inizioLoop);

    /* Prendo l'indirizzo della prossima istruzione per completare la JZERO */
    int uscitaLoop = c.indirizzoProssimaIstruzione();
    c.completaIstruzione(posizioneJZERO, uscitaLoop);
  }
}
