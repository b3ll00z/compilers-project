/************************** Programma.java ******************************
 * La classe astratta Programma definisce l'albero sintattico di un
 * programma scritto nel linguaggio fornito dalle specifiche del progetto.
 * Le sottoclassi realizzano le derivazioni date dalla grammatica
 * a partire dalla produzione iniziale.
 * Il metodo generaCodice permette di generare il codice per calcolare
 * il sottoprogramma ad esso associato.
 *************************************************************************/

import static lt.macchina.Macchina.*;

import lt.macchina.Codice;

public abstract class Programma {
  public abstract void generaCodice(Codice c);
}

/*
La seguente classe serve per creare un nodo al parse tree
in corrispondenza della derivazione:

	seqIstruzioni -> istruzione cr | seqIstruzioni istruzione cr
*/
class SequenzaIstruzioni extends Programma {
  private SequenzaIstruzioni sequenza;
  private Istruzione istruzione;

  public SequenzaIstruzioni(Istruzione istr) {
    this(null, istr);
  }

  public SequenzaIstruzioni(SequenzaIstruzioni istruzioni, Istruzione istr) {
    sequenza = istruzioni;
    istruzione = istr;
  }

  public void generaCodice(Codice c) {

    if (sequenza != null) sequenza.generaCodice(c);

    istruzione.generaCodice(c);
  }
}
